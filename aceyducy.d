/* *** GAME OF ACEY-DUCEY WRITTEN BY BILL PALMBY
   *** ADLAI STEYENSON HIGH SCHOOL, PRAIRE VIEW, ILL
   *** TRANSLATED TO RSTS/E BY DAVE AHL, DIGITAL 
   *** Translation to D by Phil Miess 
*/

import std.stdio;
import std.random;
import std.conv;
import std.string;
import std.algorithm.mutation;

int inputInt ( string prompt){
  int val ;
  writeln( prompt ~ "? ");
  string line;
  bool found = false;
  while ( !found ) {
    try {
      readf(" %d", &val);
      readln();
      found = true;
    } catch (Exception e) {
      readln();
      writeln( "? " );
    }

  }
  return val;

}

void main() {

  auto rnd = Random(unpredictableSeed);
  printHowToPlay();
  aceyducyGames(rnd);
}

void printHowToPlay()
{ 
  writeln( "Acey-ducey is played in the following manner: 
the dealer (computer) deals two cards face up.
You have the option to bet or not to bet depending
on whether or not you feel the next card will have
a value between the first two.
If you do not want to bet, input a 0. " );
}

pure @safe nothrow 
string cardToString( const int card ){
  switch ( card ) {
    default: assert( false);
    case 2 : .. case 10 : 
      return to!string( card );
    case 11 : 
      return "Jack";
    case 12 : 
      return "Queen";
    case 13 : 
      return "King";
    case 14 : 
      return "Ace";
  }

}

void printcard( const int card ){
  writeln( cardToString( card ) ); 
}

int getBet( const int stakes ) {
  const int bet = inputInt( "\nWhat is your bet");
  if ( bet <= stakes ) {
    return bet;
  }
  writeln( "Sorry, my friend, but you bet too much\n"
           "You have only ", stakes, " dollars to bet. " );
  return getBet( stakes);
}

int aceyducyHand( const int stakes, ref Random rnd) {

  writeln( "You now have ", stakes, " dollars. \n");
  int card1 = uniform(2, 15, rnd);
  int card2;
  do {
    card2 = uniform(2, 15, rnd);
  } while ( card1 == card2 );
  
  if ( card1 >= card2 ) { 
    swap( card1, card2);
  }
  printcard( card1 );
  printcard( card2 );
    
  const int bet = getBet(stakes);

  if ( bet <= 0 ) {
    writeln( "Chicken!!\n" ); 
    return stakes;
  }

  const int card3 =  uniform(2, 15, rnd);
  printcard( card3 );
  if ( card3 < card1 || card3 >= card2 ) {
    writeln( "Sorry, you lose." );
    return stakes - bet;
  } else {
    writeln( "You win!!!" );
    return stakes + bet;
  }
}

void aceyducyGame(ref Random rnd) {
  writeln();
  int stakes = 100;  
  do {
    stakes = aceyducyHand( stakes, rnd );
  } while( stakes > 0 );
}



void aceyducyGames(Random rnd) {
  aceyducyGame(rnd);
  writeln( "\nSorry, friend, but you blew your wad. " 
           "\nTry again (yes or no)? " );
  auto tryAgain = readln().strip();
  if ( tryAgain != "yes" ) { 
    writeln( "\nO. K. Hope you had fun!!" );
  } else {
    aceyducyGames(rnd);
  }
}

