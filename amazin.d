/* Amazin written by Jack Hauber
   Loomis School Windsor, CT 06095
   Translate to D by Phil Miess
*/

import std.stdio;
import std.random;
import std.conv;
import std.string;
import std.algorithm.mutation;
import std.typecons;

alias Dimentions = Tuple!(int, "H", int, "V");

Dimentions inputDimentions(){
  int x,y ;
  writeln("What are your width and length?");
  string line;
  bool found = false;
  while ( !found ) {
    try {
      readf(" %d,%d", &x, &y);

      readln();
      found = true;
    } catch (Exception e) {
      readln();
      writeln( "? " );
    }

  }
  return tuple!("H", "V")(x, y);

}

Dimentions getDimentions() {
  auto dims = inputDimentions();
  writeln();
  if ( dims.H!=1 || dims.V!=1 ) {
    return dims;
  }
  writeln( "Meaningless dimensions. Try again" );
  writeln();
  return getDimentions();    
}


void main() {
  auto rnd = Random(unpredictableSeed );
  int[25][103] W;
  int[25][103] V;
  auto dims = getDimentions();
  writeln();
  writeln();
  int Q=0;
  int Z=0;
  
  int X;
  int entranceColumn = uniform(1, dims.H+1, rnd);
  foreach( I;1 .. dims.H +1 ) {
    if ( I!=entranceColumn ) {
      write( ":--");
    } else {
      write( ":  ");
    }
  }
  writeln( ":" );
  int C=1;
  W[entranceColumn][1]=C;
  C=C+1;
  int R=entranceColumn;
  int S=1;
  goto L260;
L210 : do { 
    if ( R!=dims.H ) {
      R=R+1;
    } else if ( S!=dims.V ) {
      R=1;
      S=S+1;
    } else {
      R=1;
      S=1;
    }
L250 : } while ( W[R][S]==0 );
L260 : if ( R-1==0 ) { goto L530;}
  if ( W[R-1][S]!=0 ) { goto L530;}
  if ( S-1==0 ) { goto L390;}
  if ( W[R][S-1]!=0 ) { goto L390;}
  if ( R==dims.H ) { goto L330;}
  if ( W[R+1][S]!=0 ) { goto L330;}
  X=uniform(1, 4, rnd);
  if ( X==1 ) { goto L790;}
  if ( X==2 ) { goto L820;}
  if ( X==3 ) { goto L860;}
L330 : if ( S!=dims.V ) { goto L340;}
  if ( Z==1 ) { goto L370;}
  Q=1;
  goto L350;
L340 : if ( W[R][S+1]!=0 ) { goto L370;}
L350 : X=uniform(1, 4, rnd);
  if ( X==1 ) { goto L790;}
  if ( X==2 ) { goto L820;}
  if ( X==3 ) { goto L910;}
L370 : X=uniform(1, 3, rnd);
  if ( X==1 ) { goto L790;}
  if ( X==2 ) { goto L820;}
L390 : if ( R==dims.H ) { goto L470;}
  if ( W[R+1][S]!=0 ) { goto L470;}
  if ( S!=dims.V ) { goto L420;}
  if ( Z==1 ) { goto L450;}
  Q=1;
  goto L430;
L420 : if ( W[R][S+1]!=0 ) { goto L450;}
L430 : X=uniform(1, 4, rnd);
  if ( X==1 ) { goto L790;}
  if ( X==2 ) { goto L860;}
  if ( X==3 ) { goto L910;}
L450 : X=uniform(1, 3, rnd);
  if ( X==1 ) { goto L790;}
  if ( X==2 ) { goto L860;}
L470 : if ( S!=dims.V ) { goto L490;}
  if ( Z==1 ) { goto L520;}
  Q=1;
  goto L500;
L490 : if ( W[R][S+1]!=0 ) { goto L520;}
L500 : X=uniform(1, 3, rnd);
  if ( X==1 ) { goto L790;}
  if ( X==2 ) { goto L910;}
L520 : goto L790;
L530 : if ( S-1==0 || W[R][S-1]!=0 ) { goto L670;}
  if ( R==dims.H || W[R+1][S]!=0 ) { goto L610;}
  if ( S!=dims.V ) {
    if ( W[R][S+1]!=0 ) {
      X=uniform(1, 3, rnd);
    } else {
      X=uniform(1, 4, rnd);
    }
    if ( X==1 ) { goto L820;}
    if ( X==2 ) { goto L860;}
    if ( X==3 ) { goto L910;} 
    
  }
  if ( Z==1 ) { 
    X=uniform(1, 3, rnd);
    if ( X==1 ) { goto L820;}
    if ( X==2 ) { goto L860;}
  }
  Q=1;
  X=uniform(1, 4, rnd);
  if ( X==1 ) { goto L820;}
  if ( X==2 ) { goto L860;}
  if ( X==3 ) { goto L910;}
L610 : if ( S!=dims.V ) {
    if ( W[R][S+1]!=0 ) { goto L820;}
  }
  if ( Z==1 ) { goto L820;}
  Q=1;
  X=uniform(1, 3, rnd);
  if ( X==2 ) {
    goto L910;
  } else {
    goto L820;
  }
L670 : if ( R==dims.H || W[R+1][S]!=0 ) { 
    if ( S!=dims.V ) {
      if ( W[R][S+1]!=0 ) { goto L210;}
      goto L910; 
    }
    if ( Z==1 ) { goto L210;}
    Q=1;
    goto L910;
  }
  if ( S==dims.V ) { 
    if ( Z==1 ) { goto L860;}
    Q=1;
    goto L830;
  }
  if ( W[R][S+1]!=0 ) { goto L860;}
  X=uniform(1, 3, rnd);
  if ( X==2 ) { 
    goto L910;
  } else {
    goto L860;
  }
L790 : W[R-1][S]=C;
  C=C+1;
  V[R-1][S]=2;
  R=R-1;
  if ( C==dims.H*dims.V+1 ) { goto L1010;}
  Q=0;
  goto L260;
L820 : W[R][S-1]=C;
L830 : C=C+1;
  V[R][S-1]=1;
  S=S-1;
  if ( C==dims.H*dims.V+1 ) { goto L1010;}
  Q=0;
  goto L260;
L860 : W[R+1][S]=C;
  C=C+1;
  if ( V[R][S]==0 ) { 
    V[R][S]=2;
  } else {
    V[R][S]=3;
  }
  R=R+1;
  if ( C==dims.H*dims.V+1 ) { goto L1010;}
  goto L530;
L910 : if ( Q==1 ) { 
    Z=1;
    if ( V[R][S]==0 ) { 
      V[R][S]=1;
      Q=0;
      R=1;
      S=1;
      goto L250;
    }
    V[R][S]=3;
    Q=0;
    goto L210;
  }
  W[R][S+1]=C;
  C=C+1;
  if ( V[R][S]==0 ) { 
    V[R][S]=1;
  } else {
    V[R][S]=3;
  }
  S=S+1;
  if ( C==dims.H*dims.V+1 ) { goto L1010;}
  goto L260;
L1010 : foreach (J;1 .. dims.V+1){
	  write("I");
	  foreach(I;1 .. dims.H+1){
	    if ( V[I][J]<2 ) { 
	      write("  I");
	    } else {
              write("   ");
	    }  
          }
	  writeln();
	  foreach (I;1 .. dims.H+1){
	    if ( V[I][J]==0 || V[I][J]==2 ) {
	      write(":--");
	    } else {
              write( ":  ");
	    }  
          }
          writeln(":" );
        }
  return;

}
